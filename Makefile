include .env

## compile: Compile the binary.
compile:
	@-touch $(STDERR)
	@-rm $(STDERR)
	@-$(MAKE) -s go-compile 2> $(STDERR)
	@cat $(STDERR) | sed -e '1s/.*/\nError:\n/'  | sed 's/make\[.*/ /' | sed "/^/s/^/     /" 1>&2

clean:
	@-$(MAKE) -s go-clean
	@echo "  >  Deleting files in bin folder"
	rm -rf $(GOBIN)/*	
	
test:
	@echo "  >  Running tests"
	@GOPATH=$(GOPATH) go test ./...

## exec: Run given command, wrapped with custom GOPATH. e.g; make exec run="go test ./..."
exec: 
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) $(run)


example:
	@GOPATH=$(GOPATH) go run tool.go --url https://best-solution.fabien-ogli.workers.dev/

profile:
	@GOPATH=$(GOPATH) go run tool.go --url https://best-solution.fabien-ogli.workers.dev/links --profile 100


go-compile: go-clean go-get go-build

go-build:
	@echo "  >  Building binary..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go build -o $(GOBIN)/$(PROJECTNAME) $(GOFILES)

go-generate:
	@echo "  >  Generating dependency files..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go generate $(generate)

go-get:
	@echo "  >  Checking if there is any missing dependencies..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go get $(get)

go-install:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go install $(GOFILES)

go-clean:
	@echo "  >  Cleaning build cache"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go clean

all: compile