# Tool to test Cloudflare Worker
This project was realized to get hired by Cloudflare

## Required
* go  
* make (optionnal)  
This project was implemented with go 1.15.2  

## Functionnalities
### URL
You can do an http request with --url  
By default the request will be done in HTTPS with the standard port (ie 443),
```bash
bin/tool --url myurl
```  
If you want to specify weither it's http or https, type the following  
```bash
bin/tool --url http://myurl
```
If you want to get an http request from another port than standard, type the following
```bash
bin/tool --url https://myurl:myport/mypath
```
### PROFILE
The result to such a request will be printed on stdout  
If you want to do a profile of a website, you'll need to add the option '--profile' followed with the amount of request to execute
```bash
bin/tool --url myurl --profile nbRequest
```
## Installation
If you have an go installation directly running, do the following command  
```bash
go install gitlab.com/fabienogli/cloudflare-system
```
Or follow the compilation' section
## Compilation
### Only go
I strongly invite you to put this project under 
$GOPATH/src/repowebsite/username/
 (for me it's /home/user/go/src/gitlab.com/fabienogli/)
Execute the following command  
```bash
go build tool.go
```
### With make
Execute the following command (it will be in the bin folder with the folder name as the executable)
```bash
make compile
```

## Testing
I write basic tests to ensure not breaking everything with small changes  
To launch it with go
```bash
go test ./...
```
To launch it with make
```bash
make test
```

## Other
You can run any command using the local make configuration
```bash
make exec run="go run tool.go --url cloudflare.com"
```
## Results
### Cloudflare
![Cloudflare](img/cloudflare.png)
### Facebook
![Facebook](img/facebook.png)
### France Connect
![France connect](img/france.png)
### Google
![Google](img/google.png)
### Local Worker
![Local](img/local.png)
### Published Worker
![Own](img/own.png)
### Github Pages
![pages](img/pages.png)
### Reddit
![reddit](img/reddit.png)
### Twitter
![twitter](img/twitter.png)

Google and Cloudflare seem to be the website that has the highest error percentage.  
But it seems that error are related to the tcp connection, that has been reset.  
The amount of request within such a short time seems to trigger a back off time from this website.  
Because I wanted to be sure it was indeed http throttling, I decided to do 500 request to facebook
![500](img/500.png)
I had some errors, either an empty reponse or the tcp connection was reset, which indicate a back off time from facebook. 
So I did another test with the Cloudflare Worker with 500 requests, every answers were 200  
![rocky](img/push-it.jpg)
I had to go for the 1000 on the Cloudflare Worker
![rocky](img/1000.png)
So we can see that google.com and cloudflare.com will activate a back off time for less requests than facebook.com.
Facebook will activate it after 
But Cloudflare services like the Worker Service will activate a back off after approximately the 720th request, this time answering only with too many requests.



## Aknowledgments
Makefile was inspired by : https://kodfabrik.com/journal/a-good-makefile-for-go