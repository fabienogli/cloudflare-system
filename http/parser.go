package http

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const HttpSeparator string = "\r\n\r\n"

type link struct {
	Name string `json:"name"`
	Url string `json:"url"`
}

type links struct {
	Links []link `json:"links"`
}

type Response struct {
	Header      Header
	Body        string
	Size        int
	Start       time.Time
	TimeElapsed time.Duration
	Error       error
}


type Header struct {
	HttpVersion float64
	Message     string
	StatusCode  int
	Other       string
}

func (r *Response) StartTiming() {
	r.Start = time.Now()
}

func (r *Response) StopTiming() {
	r.TimeElapsed = time.Since(r.Start)
}

func (r Response) String() string {
	out := fmt.Sprintf("%s\n", r.Header)
	if r.Body != "" {
		out += fmt.Sprintf("%s\n", r.Body)
	}
	out +=fmt.Sprintf("Time elapsed: %f seconds\n", r.TimeElapsed.Seconds())
	if r.Error != nil {
		out += fmt.Sprintf("Error happened: %v\n", r.Error)
	}
	return out
}

func (r Response) GetLinks() (links, error) {
	links := links{}
	if !r.Header.IsJson() {
		return links, errors.New("No links")
	}
	err := json.Unmarshal([]byte(r.Body), &links)
	if IsError(err, false) {
		return links, err
	}
	return links, nil
}


func (r *Response) ParseBuffer(buf bytes.Buffer) {
	r.Size = buf.Len()
	strBuffer := buf.String()
	splitString := strings.Split(strBuffer, HttpSeparator)
	r.Header = parseHeader(splitString[0])
	if r.Header.StatusCode != 200 && r.Header.StatusCode !=0 {
		errMsg := fmt.Sprintf("%d %s", r.Header.StatusCode, r.Header.Message)
		r.Error = errors.New(errMsg)
	} else if r.Header.StatusCode == 0 {
		r.Error = errors.New("empty response")
	}
	if len(splitString) > 1 {
		r.Body = splitString[1]
	}
}

func (h Header) IsJson() bool {
	if h.Other != "" {
		return strings.Contains(h.Other, "content-type: application/json")
	}
	return false
}

func (h Header) String() string {
	out := fmt.Sprintf("HTTP/%.1f %d %s", h.HttpVersion, h.StatusCode, h.Message)
	if h.Other != "" {
		out += fmt.Sprintf("\n%s", h.Other)
	}
	return out
}

func parseHeader(rawHeader string) Header {
	header := Header{}
	split := strings.Split(rawHeader, "\n")
	if len(split) > 1 {
		header.Other = strings.Join(split[1:], "\n")
	}
	regex, err := regexp.Compile(`HTTP/(\d.\d)\s(\d*)\s(.*)`)
	if err != nil {
		return header
	}
	found := regex.FindStringSubmatch(split[0])
	if len(found) > 1 {
		header.HttpVersion, err = strconv.ParseFloat(found[1], 64)
	}
	if len(found) > 2 {
		header.StatusCode, err = strconv.Atoi(found[2])
	}
	if len(found) > 3 {
		header.Message = found[3]
	}
	return header
}