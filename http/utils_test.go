package http

import (
	"net/url"
	"strconv"
	"testing"
)

func TestFormatUrl(t *testing.T) {
	basename, httpPrefix, securePrefix := "www.cloudflare.com", "http://", "https://"
	simpleUrl, err := FormatUrl(securePrefix + basename)
	checkError(err, t)
	securedPortStr := strconv.FormatInt(SecurePort, 10)
	httpPortStr := strconv.FormatInt(Port, 10)
	compare(t, basename, securedPortStr, "/", simpleUrl)
	ddosPath := "/ddos"
	withRoute, err := FormatUrl(securePrefix + basename + ddosPath)
	checkError(err, t)
	compare(t, basename,  securedPortStr, ddosPath, withRoute)

	withRoute, err = FormatUrl(securePrefix + basename + ":" + securedPortStr + ddosPath)
	checkError(err, t)
	compare(t, basename,  securedPortStr, ddosPath, withRoute)
	withRoute, err = FormatUrl(httpPrefix + basename  + ddosPath)
	checkError(err, t)
	compare(t, basename,  httpPortStr, ddosPath, withRoute)


	shortName := "cloudflare.com"

	withRoute, err = FormatUrl(shortName + ddosPath)
	checkError(err,t)
	compare(t, shortName,  securedPortStr, ddosPath, withRoute)

	withRoute, err = FormatUrl(shortName + ":" + httpPortStr + ddosPath)
	checkError(err,t)
	compare(t, shortName,  httpPortStr, ddosPath, withRoute)

	withRoute, err = FormatUrl(shortName + ":" + "8080" + ddosPath)
	checkError(err,t)
	compare(t, shortName, "8080", ddosPath, withRoute)


	withRoute, err = FormatUrl(shortName + ":" + securedPortStr + ddosPath)
	checkError(err,t)
	compare(t, shortName, securedPortStr, ddosPath, withRoute)
}

func checkError(err error, t *testing.T) {
	if err != nil {
		t.Errorf("Error happened %s", err)
	}
}

func compare(t *testing.T, basename string, port string, path string, simpleUrl *url.URL) {
	if simpleUrl.Hostname() !=  basename {
		t.Errorf("Wrong hostname\nWanted %s, got %s",basename, simpleUrl.Hostname())
	}
	if simpleUrl.Path != path {
		t.Errorf("Wrong path\nWanted %s, got %s", path, simpleUrl.Path)

	}
	if simpleUrl.Port() != port {
		t.Errorf("Wrong Port\nWanted %s, got %s",port,  simpleUrl.Port())
	}
}