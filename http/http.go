package http

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net"
	"net/url"
)

const Port = 80
const SecurePort = 443
const SecurePrefix = "https"
const Prefix = "http"

func IsError(err error, verbose bool) bool {
	if err == nil {
		return false
	}
	if verbose {
		log.Printf("Error happened %v\n", err)
	}
	return true
}

func Request(parsedUrl *url.URL, verbose bool) Response {
	conn, err := net.Dial("tcp", parsedUrl.Host)
	if IsError(err, verbose) {
		return Response{Error: err}
	}
	return request(&conn, verbose, parsedUrl)
}


func request(conn *net.Conn, verbose bool, parsedUrl *url.URL) Response {
	Response := Response{}
	defer (*conn).Close()
	if verbose {
		log.Println("client: connected to: ", (*conn).RemoteAddr())
	}
	request := buildRequest(parsedUrl)
	if verbose {
		log.Printf("Doing the following request\n%s", request)
	}
	(&Response).StartTiming()
	_, err := fmt.Fprintf(*conn, request)
	if IsError(err, verbose) {
		Response.Error = err
		return Response
	}
	var buf bytes.Buffer
	_, err = io.Copy(&buf, *conn)
	if IsError(err, verbose) {
		Response.Error = err
		return Response
	}
	(&Response).StopTiming()
	Response.ParseBuffer(buf)
	if verbose {
		log.Printf("Received:\n%s", Response)
	}
	return Response
}

func buildRequest(parsedUrl *url.URL) string {
	requiredLine := fmt.Sprintf("GET %s HTTP/1.0\r\n", parsedUrl.Path)
	host := fmt.Sprintf("Host: %s\r\n", parsedUrl.Host)
	userAgent := "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0"
	request := requiredLine + host + userAgent + HttpSeparator
	return request
}

func SecureRequest(parsedUrl *url.URL, verbose bool) Response {
	config := tls.Config{InsecureSkipVerify: true}
	var conn net.Conn
	conn, err := tls.Dial("tcp", parsedUrl.Host, &config)
	if IsError(err, verbose) {
		return Response{Error: err}
	}
	return request(&conn, verbose, parsedUrl)
}