package http

import (
	"log"
	"net/url"
	"strings"
)

func FormatUrl(rawUrl string) (*url.URL, error) {
	if !strings.Contains(rawUrl, Prefix) {
		rawUrl = SecurePrefix +"://" + rawUrl
	}
	parsedUrl, err := url.Parse(rawUrl)
	if err != nil {
		log.Printf("Error happened while parsing url\n%v", err)
		return parsedUrl, err
	}
	if parsedUrl.Path == "" {
		parsedUrl.Path = "/"
	}
	if parsedUrl.Scheme == "" {
		//By default https
		parsedUrl.Scheme = SecurePrefix
	}
	if parsedUrl.Port() != "" {
		return parsedUrl, nil
	}
	if Prefix == parsedUrl.Scheme {
		parsedUrl.Host += ":80"
	} else {
		parsedUrl.Host += ":443"
	}
	return parsedUrl, nil
}