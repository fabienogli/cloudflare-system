package http

import (
	"bytes"
	"testing"
)

func TestResponse_BuildFromRawBytes(t *testing.T) {
	header, body := "HTTP/1.0 200 OK", "This is the body"
	buffer := bytes.NewBuffer([]byte(header +HttpSeparator + body))
	r := Response{}
	r.ParseBuffer(*buffer)
	if r.Body != body {
		t.Errorf("Want %s, got %s", body, r.Body)
	}
	if r.Header.String() != header {
		t.Errorf("Want I%sI, got I%sI", header, r.Header)
	}
}

func TestParseHeader(t *testing.T) {
	rawHeader := "HTTP/1.0 404 Not found"
	header := parseHeader(rawHeader)
	statusWanted := "Not found"
	if header.Message != statusWanted {
		t.Errorf("Wanted %s, got %s", statusWanted, header.Message)
	}
	codeWanted := 404
	if header.StatusCode != codeWanted {
		t.Errorf("Wanted %d, got %d", codeWanted, header.StatusCode)
	}
	httpVersionWanted := 1.0
	if header.HttpVersion != httpVersionWanted {
		t.Errorf("Wanted %f, got %f", httpVersionWanted, header.HttpVersion)
	}
}
