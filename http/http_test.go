package http

import (
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestGetWebsite(t *testing.T) {
	responseWanted := "OK"
	// Start a local HTTP server
	server := httptest.NewServer(simpleHandler(responseWanted))
	// Close the server when test finishes
	defer server.Close()
	parsedUrl, _ := url.Parse(server.URL)
	parsedUrl.Path = "/"
	log.Printf("this %s", parsedUrl)
	response := Request(parsedUrl, true)
	checkResponse(response, t, responseWanted)
}

func checkResponse(response Response, t *testing.T, responseWanted string) {
	if response.Error != nil {
		t.Errorf("Error happened %v", response.Error)
	}
	header := response.Header
	if header.StatusCode != 200 {
		t.Errorf("Wrong status code\nHeader status code %d, wanted 200", header.StatusCode)
	}
	if response.Body != responseWanted {
		log.Printf(response.Body)
		t.Errorf("Wrong Body\nWanted %s, got %s", responseWanted, response.Body)
	}
}

func simpleHandler(responseWanted string) http.HandlerFunc {
	handler := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		// Test request parameters
		//equals(t, req.URL.String(), "/some/path")
		// Send response to be tested
		_, _ = rw.Write([]byte(responseWanted))
	})
	return handler
}


func TestHTTPSServer(t *testing.T) {
	responseWanted := "OK"
	server := httptest.NewTLSServer(simpleHandler(responseWanted))
	log.Printf("server.URL %s", server.URL)
	parsedUrl, err := url.Parse(server.URL)
	parsedUrl.Path = "/"
	if err != nil {
		log.Fatalf("err happened here %v", err)
	}
	response := SecureRequest(parsedUrl, true)
	checkResponse(response, t, responseWanted)
}