package main

import (
	"flag"
	"fmt"
	"gitlab.com/fabienogli/cloudflare-system/http"
	"log"
	"net/url"
	"os"
	"sort"
)


func testWebsite(c chan http.Response, url *url.URL) {
	var response http.Response
	if url.Scheme == http.Prefix {
		response = http.Request(url, false)
	} else {
		response = http.SecureRequest(url, false)
	}
	c <- response
}

func profile(url *url.URL, nbRequest int) {
	Responses := make([]http.Response, nbRequest)
	c := make(chan http.Response)
	fmt.Println("Launching requested")
	for i:=0; i < nbRequest; i++ {
		go testWebsite(c, url)
	}
	fmt.Printf("Missing %d Responses", nbRequest)
	for i:=0; i < nbRequest; i++ {
		fmt.Print("\033[G\033[K")
		fmt.Printf("Missing %d Responses", nbRequest -i-1)
		Responses[i] = <- c
	}
	fmt.Print("\033[G\033[K")
	fmt.Print("All Responses fetched, formating Responses")

	s := FormatResponses(Responses)
	fmt.Print("\033[G\033[K")
	fmt.Println()
	fmt.Print(s)
}

func FormatResponses(Responses []http.Response) string {
	nbRequest := len(Responses)
	smallestResponseSize, biggestResponseSize, errorCount := 100000, 0, 0
	fastestTime, slowestTime, cumulated := 1000000.0, 0.0, 0.0
	out := fmt.Sprintf("Number of requests:\t%d\n", nbRequest)
	times := make([]float64, nbRequest)
	errors := make(map[int]string)
	for i := 0; i < nbRequest; i++ {
		times[i] = Responses[i].TimeElapsed.Seconds()
		cumulated += times[i]
		if Responses[i].Error != nil {
			header := Responses[i].Header
			errors[header.StatusCode] = fmt.Sprintf("%s", Responses[i].Error)
			errorCount ++
			//because there is an error, not counting time or size
			continue
		}
		if Responses[i].Size > biggestResponseSize {
			biggestResponseSize = Responses[i].Size
		}
		if Responses[i].Size < smallestResponseSize {
			smallestResponseSize = Responses[i].Size
		}
		if times[i] > slowestTime {
			slowestTime = times[i]
		}
		if times[i] < fastestTime {
			fastestTime = times[i]
		}
		
	}
	errorPercent := float32(errorCount)/ float32(nbRequest)
	out += fmt.Sprintf("Percent of error:\t%.2f%%\n", errorPercent*100)
	out += fmt.Sprintf("Fastest Response:\t%f s\n", fastestTime)
	out += fmt.Sprintf("Slowest Response:\t%f s\n", slowestTime)
	out += fmt.Sprintf("Biggest Response Size:\t%d B\n", biggestResponseSize)
	out += fmt.Sprintf("Smallest Response Size:\t%d B\n", smallestResponseSize)
	sort.Float64s(times)
	middle := (nbRequest / 2) -1
	var median float64
	if nbRequest % 2 == 0 {
		median = (times[middle] + times[middle + 1]) / 2
	} else {
		median = times[middle]
	}
	out += fmt.Sprintf("Median time:\t\t%f s\n", median)
	mean := cumulated / float64(nbRequest)
	out += fmt.Sprintf("Mean time:\t\t%f s\n", mean)
	if len(errors) > 0 {
		out += fmt.Sprintln("Errors:")
	}
	for statusCode, err := range errors  {
		if statusCode == 0 {
			out += fmt.Sprintf("Error with connection->%s\n", err)
		} else {
			out += fmt.Sprintf("Status code %d, error is:%s\n", statusCode, err)
		}
	}
	return out
}


func main() {
	urlPtr := flag.String("url", "", "Url to request (Required)")
	profilePtr := flag.Int("profile", 0, "Profile the url requested N times")
	flag.Parse()

	if *urlPtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	parsedUrl, err := http.FormatUrl(*urlPtr)
	if err != nil {
		log.Fatalf("Error happened while parsing %s\n%v", *urlPtr, err)
	}

	if *profilePtr != 0 {
		profile(parsedUrl, *profilePtr)
		return
	}
	if parsedUrl.Scheme == http.Prefix {
		http.Request(parsedUrl, true)
	} else {
		http.SecureRequest(parsedUrl, true)
	}
}
